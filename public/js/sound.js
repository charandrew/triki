var music=new Audio("snd/bso.ogg");

function playMusic(){
    music.play();
    music.volume=0.4;
    music.loop="true";
}
function playP1pop(){
    var audio=new Audio("snd/p1Pop.mp3");
    music.volume=0.5;
    audio.play();
}
function playP2pop(){
    var audio=new Audio("snd/p2Pop.mp3");
    music.volume=0.5;
    audio.play();
}
function playPlayer1(){
    var audio=new Audio("snd/p1.mp3");
    audio.play();
}
function playPlayer2(){
    var audio=new Audio("snd/p2.mp3");
    audio.play();
}
function playTheWinner(){
    var audio=new Audio("snd/winner.mp3");
    audio.play();
}
function playCelebration(){
    var audio=new Audio("snd/celebration.mp3");
    audio.volume=0.7;
    audio.play();
}
function playYouWin(){
    var audio=new Audio("snd/Win.mp3");
    audio.play();
}
function playTie(){
    var audio=new Audio("snd/tie.mp3");
    audio.play();
}
function playPrepare(){
    var audio=new Audio("snd/prepare.mp3");
    audio.play();
}
function playPop(){
    var audio=new Audio("snd/pop.mp3");
    audio.volume=0.4;
    audio.play();
}
function playGameOver(){
    var audio=new Audio("snd/gameOver.mp3");
    audio.play();
}
function stopMusic(){
    var elemento = document.getElementById("muted");
    if(elemento.innerText=="Music ON"){
        elemento.innerText="Music OFF";
        elemento.style.boxShadow="inset 0 0 2vw red";
        music.pause();
    }else{
        elemento.innerText="Music ON";
        elemento.style.boxShadow="inset 0 0 2vw chartreuse";
        music.play();
    }
}