
var turn=0; // contador de turnos

var cordCasilla = new Object();
cordCasilla.id="c1";
cordCasilla.x =0;
cordCasilla.y=0;


var column1 = new Object(); //c1,c4,c7
column1.p1=0;
column1.p2=0;

var column2 = new Object(); //c2,c5,c8
column2.p1=0;
column2.p2=0;

var column3 = new Object(); //c3,c6,c9
column3.p1=0;
column3.p2=0;

var row1 = new Object(); //c1,c2,c3
row1.p1=0;
row1.p2=0;

var row2 = new Object(); //c4,c5,c6
row2.p1=0;
row2.p2=0;

var row3 = new Object(); //c7,c8,c9
row3.p1=0;
row3.p2=0;

var diag1 = new Object(); //c1,c5,c9
diag1.p1=0;
diag1.p2=0;

var diag2 = new Object(); //c3,c5,c7
diag2.p1=0;
diag2.p2=0;

var playerTurn=0; // 0= player1 , 1=player2

var casillas = document.querySelectorAll('.image');

var msg=document.getElementById("msg");

var cont=0;

var someomeWin=false;

var p1name="Player 1";
var p2name="Player 2";

var p1score=0;
var p2score=0;
var round=1;

var clickCasilla=false;

function reiniciar(){
    for(var i = 0; i < casillas.length; i++) {
        casillas[i].src ="img/vacio.png";
        casillas[i].alt ="vacia";
    }
    column1.p1=0;column1.p2=0;column2.p1=0;column2.p2=0;column3.p1=0;column3.p2=0;
    row1.p1=0; row1.p2=0;row2.p1=0;row2.p2=0;row3.p1=0;row3.p2=0;
    diag1.p1=0;diag1.p2=0;diag2.p1=0;diag2.p2=0;
    turn=0,playerTurn=0;cont=0;
    msg.innerText=p1name+", is your Turn";
    estilo("red");
    msg.style.color = "blanchedalmond";
    document.getElementById("player2").style.backgroundColor="black"
    document.getElementById("player1").style.backgroundColor="black"
    document.getElementById("player1").style.boxShadow="none";
    document.getElementById("player2").style.boxShadow="none";
    document.getElementById("player2Imagen").src="img/player2.gif";
    document.getElementById("player1Imagen").src="img/player1.gif";
    document.getElementById("player1").style.backgroundImage="url('img/hell.gif')";
    document.getElementById("player2").style.backgroundImage="url('img/water.gif')";
    document.getElementById("effect").style.opacity="1";
    document.getElementById("effect2").style.opacity="1";
    someomeWin=false;
}

function estilo(color){
    document.getElementById("msg").style.boxShadow="inset 0 0 2vw "+color;
}

function animacion(){
    var id = setInterval(function(){
        var r = Math.floor(Math.random()*255);
        var g = Math.floor(Math.random()*255);
        var b = Math.floor(Math.random()*255);
        var color = "RGB("+r+","+g+","+b+")";
        msg.style.color = color;
        if(playerTurn==0){
            document.getElementById("player2").style.backgroundColor=color;
            document.getElementById("player2").style.boxShadow=" 0 0 2vw "+color;
        }else{
            document.getElementById("player1").style.backgroundColor=color;
            document.getElementById("player1").style.boxShadow=" 0 0 2vw "+color;
        }
        cont++;
        if(cont==25){
            clearInterval(id);
        }
    },100);

}

function manejadorCallback(evento) {
    
    if(evento.target.alt != "vacia" || someomeWin){return;}else{evento.target.alt ="llena";} // si la casilla no está vacía, no haga nada
    
    turn+=1;
    
    cordCasilla.id = evento.target.id;
    cordCasilla.x = evento.target.getBoundingClientRect().left + 0.5*(evento.target.clientWidth);
    cordCasilla.y = evento.target.getBoundingClientRect().top + 0.5*(evento.target.clientWidth);

    clickCasilla=true;


    switch(evento.target.id){
        case "c1":
            if(playerTurn==0){column1.p1+=1;row1.p1+=1;diag1.p1+=1;}
            else{column1.p2+=1;row1.p2+=1;diag1.p2+=1;}
            break;
        case "c2":
            if(playerTurn==0){column2.p1+=1;row1.p1+=1;}
            else{column2.p2+=1;row1.p2+=1;}
            break;
        case "c3":
            if(playerTurn==0){column3.p1+=1;row1.p1+=1;diag2.p1+=1;}
            else{column3.p2+=1;row1.p2+=1;diag2.p2+=1;}
            break;
        case "c4":
            if(playerTurn==0){column1.p1+=1;row2.p1+=1;}
            else{column1.p2+=1;row2.p2+=1;}
            break;
        case "c5":
            if(playerTurn==0){column2.p1+=1;row2.p1+=1;diag1.p1+=1;diag2.p1+=1;}
            else{column2.p2+=1;row2.p2+=1;diag1.p2+=1;diag2.p2+=1;}
            break;
        case "c6":
            if(playerTurn==0){column3.p1+=1;row2.p1+=1;}
            else{column3.p2+=1;row2.p2+=1;}
            break;
        case "c7":
            if(playerTurn==0){column1.p1+=1;row3.p1+=1;diag2.p1+=1;}
            else{column1.p2+=1;row3.p2+=1;diag2.p2+=1;}
            break;
        case "c8":
            if(playerTurn==0){column2.p1+=1;row3.p1+=1;}
            else{column2.p2+=1;row3.p2+=1;}
            break;
        case "c9":
            if(playerTurn==0){column3.p1+=1;row3.p1+=1;diag1.p1+=1;}
            else{column3.p2+=1;row3.p2+=1;diag1.p2+=1;}
            break;
    }

    if(playerTurn==0){
        document.getElementById("msg").innerText=p2name+", is your Turn";estilo("rgb(0,255,255)");
        setTimeout(function funcion(){evento.target.src = "img/fireball.gif";},400)
        playP1pop();
        playerTurn=1;
    }else{
        document.getElementById("msg").innerText=p1name+", is your Turn";estilo("red");
        setTimeout(function funcion(){evento.target.src = "img/ice.gif";},400)
        playP2pop();
        playerTurn=0;
    }


    if(column1.p1==3 || column2.p1==3 || column3.p1==3 || column1.p2==3 || column2.p2==3 || column3.p2==3 ||
        row1.p1==3 || row2.p1==3 || row3.p1==3 || row1.p2==3 || row2.p2==3 || row3.p2==3 ||
        diag1.p1==3 || diag2.p1==3 ||  diag1.p2==3 || diag2.p2==3 ){
            someomeWin=true;
            document.getElementById("player1").style.backgroundImage="none";
            document.getElementById("player2").style.backgroundImage="none";
            if(playerTurn==0){
                document.getElementById("msg").innerText="Congrats! "+p2name+" WINS!";
                setTimeout(function funcion(){
                    document.getElementById("player2Imagen").src="img/player2win.gif";},400);
                document.getElementById("player1Imagen").src="img/player1Lose.gif";
                document.getElementById("player1").style.backgroundColor="white";
                document.getElementById("effect").style.opacity="0";
                document.getElementById("effect2").style.opacity="0";
                setTimeout(playYouWin,1200);playPlayer2();playCelebration();
                estilo("rgb(0,255,255)");animacion();
                p2score+=1;
                document.getElementById("p2s").innerText=p2score;
            }else{
                document.getElementById("msg").innerText="Congrats! "+p1name+" WINS!";
                setTimeout(function funcion(){
                    document.getElementById("player1Imagen").src="img/player1win.gif";
                },400);
                document.getElementById("player2Imagen").src="img/player2Lose.gif";
                document.getElementById("player2").style.backgroundColor="white";
                document.getElementById("effect").style.opacity="0";
                document.getElementById("effect2").style.opacity="0";
                setTimeout(playYouWin,1200);playPlayer1();playCelebration();
                estilo("red");animacion();
                p1score+=1;
                document.getElementById("p1s").innerText=p1score;
            }
            round+=1;
            document.getElementById("round").innerText=round;
            setTimeout(reiniciar, 3000);
            document.getElementById("effect").style.opacity="0";
            document.getElementById("effect2").style.opacity="0";
        }
    if(turn==9 && !someomeWin){   
        setTimeout(reiniciar, 3000);
        document.getElementById("msg").innerText="IT'S A TIE! :(";estilo("yellow");
        playTie();
    }
}

function cambiarNombre(evento){
    if(evento.target.id=="p1name"){
        p1name=prompt("Enter the new player Name");
        if(p1name!=null){evento.target.innerText = p1name;}else{p1name="Player 1"}
    }else{
        p2name=prompt("Enter the new player Name");
        if(p2name!=null){evento.target.innerText = p2name;}else{p2name="Player 2"}
    }
    if(playerTurn==0){
        document.getElementById("msg").innerText=p1name+", is your Turn";
    }else{document.getElementById("msg").innerText=p2name+", is your Turn";}
    
}


function inicializar(){
    
    for(var i = 0; i < casillas.length; i++) {
        casillas[i].addEventListener('click', manejadorCallback);
        casillas[i].addEventListener('mouseover', playPop);
        casillas[i].src ="img/vacio.png";
        casillas[i].alt ="vacia";
    }
    document.getElementById("mesage").style.display="none";
    playPrepare();
    playMusic();
    document.getElementById("p1name").addEventListener('click',cambiarNombre);
    document.getElementById("p2name").addEventListener('click',cambiarNombre);
    document.getElementById("reset").addEventListener('mouseover', playPop);
    document.getElementById("muted").addEventListener('mouseover', playPop);
    document.getElementById("msg").innerText=p1name+", is your Turn";
    document.getElementById("effect").style.opacity="1";
    document.getElementById("effect2").style.opacity="1"; 
    glow();
}  
   

function resetCounters(){
    gameOver();
    if(p1score!=p2score){playTheWinner();}else{playTie();}
    setTimeout(() => {
        playGameOver();
    }, 1500);
    setTimeout(() => {
        reiniciar()
    }, 3000);;    
    p1score=0;
    p2score=0;
    round=1;    
    document.getElementById("p2s").innerText=p2score
    document.getElementById("p1s").innerText=p1score
    document.getElementById("round").innerText=round;
}
