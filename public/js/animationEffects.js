var laser = document.getElementById("laser");
var laser2 = document.getElementById("laser2");
var wave = document.getElementById("wave");
var xLaser = laser.getBoundingClientRect().left;
var yLaser = laser.getBoundingClientRect().top;        
var xLaser2 = laser2.getBoundingClientRect().left;
var yLaser2 = laser2.getBoundingClientRect().top;  
var laser2Width = laser2.clientWidth;      
var hip=0;
var ladoa=0;
var ladob=0;
var angulo=0;
document.addEventListener("click",shut);
var textosBrillantes = document.querySelectorAll('.textGlow');

function glow(){
    for(var i = 0; i < textosBrillantes.length; i++) {
        var color = textosBrillantes[i].style.color;
        textosBrillantes[i].style.textShadow="0 0 0.3vw black,0 0 0.3vw "+color+", 0 0 0.8vw yellow"
    }
}

function gameOver(){
    var casilla = document.getElementById("c1");
    var ventana = document.getElementById("mesage");
    var imagenFondo = document.getElementById("windowImage");
    var msgWinner = document.getElementById("theWinner");

    ventana.style.width=(casilla.clientWidth*3+(casilla.clientWidth*0.069))+"px";
    ventana.style.height=(casilla.clientWidth*3+(casilla.clientWidth*0.069))+"px";
    ventana.style.left=casilla.getBoundingClientRect().left+"px";
    ventana.style.top=casilla.getBoundingClientRect().top+"px";
    msgWinner.style.display="block";
    msgWinner.style.zIndex="100";
    msgWinner.style.fontSize="5vw";
    msgWinner.style.width=ventana.style.width;
    msgWinner.style.color="white";
    msgWinner.style.height=casilla.clientWidth+"px";
    msgWinner.style.left=casilla.getBoundingClientRect().left+"px";
    msgWinner.style.top=(casilla.getBoundingClientRect().top+casilla.clientWidth)+"px";

    document.getElementById("boton").style.display="none";
    ventana.style.display="block";
    imagenFondo.style.width="100%"
    imagenFondo.style.height="100%"
    imagenFondo.style.padding="0";
    imagenFondo.style.margin="auto";
    
    if(p1score>p2score){
        imagenFondo.src="img/explode2.gif";
        msgWinner.innerText=p1name+"\nThe Winner";
        msgWinner.style.color="red";
        document.getElementById("msg").style.color="red";
        document.getElementById("msg").style.boxShadow="inset 0 0 2vw red";
        document.getElementById("msg").innerText="P1, You were the BEST!!";
    }else{
        if(p1score<p2score){
            imagenFondo.src="img/explode.gif";
            msgWinner.innerText=p2name+"\nThe Winner";
            msgWinner.style.color="rgb(0,255,255)";
            document.getElementById("msg").style.color="rgb(0,255,255)";
            document.getElementById("msg").style.boxShadow="inset 0 0 2vw rgb(0,255,255)";
            document.getElementById("msg").innerText="P2, You were the BEST!!";
        }else{
            imagenFondo.src="img/fireIce.gif";
            msgWinner.innerText="Really?\nwas a TIE!";
            document.getElementById("msg").style.color="rgb(255,255,255)";
            document.getElementById("msg").style.boxShadow="inset 0 0 2vw rgb(255,255,255)";
            document.getElementById("msg").innerText="Buuuuuuuuu!!";            
        }
    }
    setTimeout(() => {
        ventana.style.display="none";
        msgWinner.style.display="none";
    }, 3000);
}


function shut(){

    if (clickCasilla && playerTurn==1){

        var explosion = document.getElementById("explosion");
        var explosion2 = document.getElementById("explosion2");
        var casilla = document.getElementById(cordCasilla.id);
        var casillaX = casilla.getBoundingClientRect().left;
        var casillaY = casilla.getBoundingClientRect().top;

        wave.style.display="block";
        wave.animate([
            {boxShadow:'0 0 1vw red, 0 0 2vw red, 0 0 3vw red, 0 0 4vw red'},
            {boxShadow:"0 0 3vw 3vw red, 0 0 4vw 4vw orangered, 0 0 6vw 6vw orange, 0 0 8vw 8vw orange, 0 0 10vw 10vw yellow"},
            {boxShadow:'0 0 1vw red, 0 0 2vw red, 0 0 3vw red, 0 0 4vw red'}
        ],{
            duration:150,
            easing:'ease-in-out',
            iterations:2,
            directions:'alternate',
            fill:'forwards'
        });

        laser.style.opacity="1";
        laser.animate([
            {boxShadow:'0 0 0.1vw red, 0 0 0.2vw red, 0 0 0.0.3vw red, 0 0 0.4vw red'},
            {boxShadow:"0 0 0.5vw 0.5vw yellow, 0 0 0.7vw 0.7vw orange, 0 0 0.8vw 0.8vw orangered, 0 0 1vw 1vw red, 0 0 1.5vw 1.5vw red"},
            {boxShadow:'0 0 0.1vw red, 0 0 0.2vw red, 0 0 0.3vw red, 0 0 0.4vw red'}
        ],{
            duration:100,
            easing:'ease-in-out',
            iterations:3,
            directions:'alternate',
            fill:'forwards'
        });
        ladoa = cordCasilla.y - yLaser;
        ladob = cordCasilla.x - xLaser;
        hip = Math.hypot(ladoa,ladob);
        angulo = Math.asin(ladoa/hip)*(180/Math.PI);
        laser.style.width=hip+"px";
        laser.style.transformOrigin = "0 0";
        laser.style.transform="rotate("+angulo+"deg)";

        setTimeout(function funcion(){
            wave.style.display="none";laser.style.opacity="0";
        },300);


        explosion.style.display = "block";
        explosion.style.width=casilla.clientWidth + "px";
        explosion.style.top=casillaY + "px";
        explosion.style.left=casillaX + "px";

        explosion.animate([
            {
                boxShadow:'0 0 2vw red, 0 0 3vw red, 0 0 4vw red, 0 0 5vw red',
                width:"0px", height:"0px",
                top:parseInt(casillaY+0.5*casilla.clientWidth)+"px",
                left:parseInt(casillaX+0.5*casilla.clientWidth)+"px"
            },
            {
                boxShadow:"0 0 3vw 3vw red, 0 0 4vw 4vw orangered, 0 0 6vw 6vw orange, 0 0 8vw 8vw orange, 0 0 10vw 10vw yellow",
                width:casilla.clientWidth + "px",height:casilla.clientWidth + "px",
                top:parseInt(casillaY)+"px",
                left:parseInt(casillaX)+"px"
            },
            {
                boxShadow:'0 0 1vw red, 0 0 2vw red, 0 0 3vw red, 0 0 4vw red',
                width:"0px", height:"0px",
                top:parseInt(casillaY+0.5*casilla.clientWidth)+"px",
                left:parseInt(casillaX+0.5*casilla.clientWidth)+"px"
            }
        ],{
            duration:1000,
            easing:'ease-in-out',
            iterations:1,
            directions:'alternate',
            fill:'forwards'
        });
        setTimeout(function funcion(){explosion.style.display="none";},400);
        clickCasilla=false;

    }
    if (clickCasilla && playerTurn==0){

        var explosion = document.getElementById("explosion");
        var explosion2 = document.getElementById("explosion2");
        var casilla = document.getElementById(cordCasilla.id);
        var casillaX = casilla.getBoundingClientRect().left;
        var casillaY = casilla.getBoundingClientRect().top;
        
        wave2.style.display="block";
        wave2.animate([
            {boxShadow:'0 0 0.1vw rgb(0,255,255), 0 0 0.2vw rgb(0,255,255), 0 0 0.3vw rgb(0,255,255), 0 0 0.4vw rgb(0,255,255)'},
            {boxShadow:"0 0 0.5vw 0.5vw rgb(0,255,255), 0 0 0.7vw 0.7vw lime, 0 0 0.8vw 0.8vw lime, 0 0 1vw 1vw lime, 0 0 1.5vw 1.5vw yellow"},
            {boxShadow:'0 0 0.1vw rgb(0,255,255), 0 0 0.2vw rgb(0,255,255), 0 0 0.3vw rgb(0,255,255), 0 0 0.4vw rgb(0,255,255)'}
        ],{
            duration:150,
            easing:'ease-in-out',
            iterations:2,
            directions:'alternate',
            fill:'forwards'
        });

        ladoa =yLaser2-cordCasilla.y ;
        ladob =(xLaser2 + laser2Width)-cordCasilla.x ;
        hip = Math.hypot(ladoa,ladob);
        angulo = Math.asin(ladoa/hip)*(180/Math.PI);
        laser2.style.width=hip+"px";
        laser2.style.transformOrigin ="right top";
        laser2.style.transform="rotate("+angulo+"deg)";

        laser2.style.opacity="1";
        laser2.animate([
            {boxShadow:'0 0 0.3vw rgb(0,255,255), 0 0 0.6vw rgb(0,255,255), 0 0 0.8vw rgb(0,255,255), 0 0 1vw rgb(0,255,255)'},
            {boxShadow:"0 0 1vw 1vw rgb(0,255,255), 0 0 2vw 2vw lime, 0 0 3vw 3vw lime, 0 0 4vw 4vw lime, 0 0 5vw 5vw yellow"},
            {boxShadow:'0 0 0.3vw rgb(0,255,255), 0 0 0.6vw rgb(0,255,255), 0 0 0.8vw rgb(0,255,255), 0 0 1vw rgb(0,255,255)'}
        ],{
            duration:100,
            easing:'ease-in-out',
            iterations:3,
            directions:'alternate',
            fill:'forwards'
        });

        setTimeout(function funcion(){
            laser2.style.opacity="0";
            wave2.style.display="none";
        },300);


        explosion2.style.display = "block";
        explosion2.style.width=casilla.clientWidth + "px";
        explosion2.style.top=casillaY + "px";
        explosion2.style.left=casillaX + "px";

        explosion2.animate([
            {
                boxShadow:'0 0 2vw rgb(0,255,255), 0 0 3vw rgb(0,255,255), 0 0 4vw rgb(0,255,255), 0 0 5vw rgb(0,255,255)',
                width:"0px", height:"0px",
                top:parseInt(casillaY+0.5*casilla.clientWidth)+"px",
                left:parseInt(casillaX+0.5*casilla.clientWidth)+"px"
            },
            {
                boxShadow:"0 0 3vw 3vw rgb(0,255,255), 0 0 4vw 4vw lime, 0 0 6vw 6vw lime, 0 0 8vw 8vw yellow, 0 0 10vw 10vw yellow",
                width:casilla.clientWidth + "px",height:casilla.clientWidth + "px",
                top:parseInt(casillaY)+"px",
                left:parseInt(casillaX)+"px"
            },
            {
                boxShadow:'0 0 1vw rgb(0,255,255), 0 0 2vw rgb(0,255,255), 0 0 3vw rgb(0,255,255), 0 0 4vw rgb(0,255,255)',
                width:"0px", height:"0px",
                top:parseInt(casillaY+0.5*casilla.clientWidth)+"px",
                left:parseInt(casillaX+0.5*casilla.clientWidth)+"px"
            }
        ],{
            duration:1000,
            easing:'ease-in-out',
            iterations:1,
            directions:'alternate',
            fill:'forwards'
        });
        setTimeout(function funcion(){explosion2.style.display="none";},400);
        clickCasilla=false;

    }
}

